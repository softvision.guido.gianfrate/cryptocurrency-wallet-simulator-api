package com.softvision.crypto.integration;

import com.softvision.crypto.wallets.dto.DeleteResponseDTO;
import com.softvision.crypto.wallets.dto.WalletDTO;
import com.softvision.crypto.wallets.helpers.ConnectionToApi;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


@RunWith(SpringRunner.class)
public class WalletServiceTest {

    String BASE_URL = "http://localhost:8080/api";

    @Test
    public void getAllWallets(){
        ConnectionToApi connectionToApi = new ConnectionToApi();
        ResponseEntity res = connectionToApi.connect(BASE_URL+"/wallets", HttpMethod.GET,List.class,null);
        ArrayList listAll = (ArrayList) res.getBody();
        Assert.assertTrue(listAll != null);
        Assert.assertTrue(res.getStatusCode() == HttpStatus.OK);
    }

    @Test
    public void addNewWallet(){
        String nameWalletForTest = "Wallet for testing";
        HashMap coinPriceForTest = new HashMap();
        coinPriceForTest.put("ARS",1000.0);

        WalletDTO walletDTO = new WalletDTO();
        walletDTO.setName(nameWalletForTest);
        walletDTO.setCoinPrice(coinPriceForTest);

        ConnectionToApi connectionToApi = new ConnectionToApi();
        ResponseEntity<WalletDTO> res = connectionToApi.connect(BASE_URL+"/wallets", HttpMethod.POST,WalletDTO.class,walletDTO);
        WalletDTO walletDTOForTest = res.getBody();

        Assert.assertTrue(walletDTOForTest.getCoinPrice().get("ARS").equals(coinPriceForTest.get("ARS")));
        Assert.assertTrue(walletDTO.getName().equals(nameWalletForTest));

        //ROLLBACK
        connectionToApi.connect(BASE_URL+"/wallets/"+walletDTOForTest.getId(), HttpMethod.DELETE,WalletDTO.class,null);
    }

    @Test
    public void addNewWalletWithoutName(){

        HashMap coinPriceForTest = new HashMap();
        coinPriceForTest.put("ARS",1000.0);

        WalletDTO walletDTO = new WalletDTO();
        walletDTO.setCoinPrice(coinPriceForTest);
        ConnectionToApi connectionToApi = new ConnectionToApi();

        boolean throwException = false;

        try {
            ResponseEntity<WalletDTO> res = connectionToApi.connect(BASE_URL + "/wallets", HttpMethod.POST, WalletDTO.class, walletDTO);
        }catch (HttpClientErrorException.BadRequest ex){

            throwException = true;
        }
        Assert.assertTrue(throwException);
    }

    @Test
    public void addNewWalletWithNotExistentCoin(){
        String nameWalletForTest = "Wallet for testing";
        HashMap coinPriceForTest = new HashMap();
        coinPriceForTest.put("efwewefw",1000.0);

        WalletDTO walletDTO = new WalletDTO();
        walletDTO.setCoinPrice(coinPriceForTest);
        walletDTO.setName(nameWalletForTest);
        ConnectionToApi connectionToApi = new ConnectionToApi();

        boolean throwException = false;

        try {
            ResponseEntity<WalletDTO> res = connectionToApi.connect(BASE_URL + "/wallets", HttpMethod.POST, WalletDTO.class, walletDTO);
        }catch (HttpClientErrorException.BadRequest ex){

            throwException = true;
        }
        Assert.assertTrue(throwException);









    }


    @Test
    public void getWalletById(){

        String nameWalletForTest = "Wallet for testing";
        HashMap coinPriceForTest = new HashMap();
        coinPriceForTest.put("ARS",1000.0);

        WalletDTO walletDTO = new WalletDTO();
        walletDTO.setName(nameWalletForTest);
        walletDTO.setCoinPrice(coinPriceForTest);

        ConnectionToApi connectionToApi = new ConnectionToApi();
        ResponseEntity<WalletDTO> res = connectionToApi.connect(BASE_URL+"/wallets", HttpMethod.POST,WalletDTO.class,walletDTO);
        WalletDTO walletDTOCreated = res.getBody();

        ResponseEntity<WalletDTO> walletCreatedObteinedById = connectionToApi.connect(BASE_URL+"/wallets/"+walletDTOCreated.getId(), HttpMethod.GET,WalletDTO.class,null);
        WalletDTO walletDTOFromResponse = res.getBody();
        Assert.assertTrue(walletDTOFromResponse.equals(walletDTOCreated));

        //ROLLBACK
        connectionToApi.connect(BASE_URL+"/wallets/"+walletDTOFromResponse.getId(), HttpMethod.DELETE,WalletDTO.class,null);
    }

    public void getWalletByNotExistentId(){

        ConnectionToApi connectionToApi = new ConnectionToApi();
        try {
            ResponseEntity<WalletDTO> walletCreatedObteinedById = connectionToApi.connect(BASE_URL + "/wallets/" + 3948573958L, HttpMethod.GET, WalletDTO.class, null);
        }catch(HttpClientErrorException.NotFound ex){
            Assert.assertTrue(true);
        }
        Assert.assertTrue(false);

    }

    @Test
    public void updateWallet(){

        String nameWalletForTest = "Wallet for testing";
        HashMap coinPriceForTest = new HashMap();
        coinPriceForTest.put("ARS",1000.0);

        WalletDTO walletDTO = new WalletDTO();
        walletDTO.setName(nameWalletForTest);
        walletDTO.setCoinPrice(coinPriceForTest);

        ConnectionToApi connectionToApi = new ConnectionToApi();
        ResponseEntity<WalletDTO> resCreatedWallet = connectionToApi.connect(BASE_URL+"/wallets", HttpMethod.POST,WalletDTO.class,walletDTO);
        WalletDTO walletDTOCreated = resCreatedWallet.getBody();

        //update
        WalletDTO walletDTOBodyForUpdate = new WalletDTO();
        walletDTOBodyForUpdate.setName("Wallet Updated");
        HashMap coinPriceForUpdate = new HashMap();
        coinPriceForUpdate.put("USD",100.0);
        walletDTOBodyForUpdate.setCoinPrice(coinPriceForUpdate);

        ResponseEntity<WalletDTO> resWalletUpdate = connectionToApi.connect(BASE_URL+"/wallets/"+walletDTOCreated.getId(), HttpMethod.PUT,WalletDTO.class,walletDTOBodyForUpdate);
        WalletDTO walletUpdated = resWalletUpdate.getBody();

        Assert.assertTrue(walletUpdated.getId() == walletDTOCreated.getId());
        Assert.assertTrue(walletUpdated.getName().equals(walletDTOBodyForUpdate.getName()));
        Assert.assertTrue(walletUpdated.getCoinPrice().get("ARS") == null );

        //ROLLBACK
        connectionToApi.connect(BASE_URL+"/wallets/"+walletUpdated.getId(), HttpMethod.DELETE,WalletDTO.class,null);

    }

    @Test
    public void UpdateWalletByNotExistentId(){

        String nameWalletForTest = "Wallet for testing";
        HashMap coinPriceForTest = new HashMap();
        coinPriceForTest.put("ARS",1000.0);

        WalletDTO walletDTO = new WalletDTO();
        walletDTO.setName(nameWalletForTest);
        walletDTO.setCoinPrice(coinPriceForTest);

        ConnectionToApi connectionToApi = new ConnectionToApi();
        ResponseEntity<WalletDTO> resCreatedWallet = connectionToApi.connect(BASE_URL+"/wallets", HttpMethod.POST,WalletDTO.class,walletDTO);
        WalletDTO walletDTOCreated = resCreatedWallet.getBody();

        //update
        WalletDTO walletDTOBodyForUpdate = new WalletDTO();
        walletDTOBodyForUpdate.setName("Wallet Updated");
        HashMap coinPriceForUpdate = new HashMap();
        coinPriceForUpdate.put("USD",100.0);
        walletDTOBodyForUpdate.setCoinPrice(coinPriceForUpdate);

        boolean throwsAnError = false;
        try {
            ResponseEntity<WalletDTO> resWalletUpdate = connectionToApi.connect(BASE_URL + "/wallets/" + 20843729384729L, HttpMethod.PUT, WalletDTO.class, walletDTOBodyForUpdate);
        }catch (HttpClientErrorException.NotFound ex){
            throwsAnError=true;
        }

        Assert.assertTrue(throwsAnError);



        deleteCustomWallet(walletDTOCreated.getId());
    }


    @Test
    public void updateWalletWithNoName(){


        String nameWalletForTest = "Wallet for testing";
        HashMap coinPriceForTest = new HashMap();
        coinPriceForTest.put("ARS",1000.0);

        WalletDTO walletDTO = new WalletDTO();
        walletDTO.setName(nameWalletForTest);
        walletDTO.setCoinPrice(coinPriceForTest);

        ConnectionToApi connectionToApi = new ConnectionToApi();
        ResponseEntity<WalletDTO> resCreatedWallet = connectionToApi.connect(BASE_URL+"/wallets", HttpMethod.POST,WalletDTO.class,walletDTO);
        WalletDTO walletDTOCreated = resCreatedWallet.getBody();

        //update
        WalletDTO walletDTOBodyForUpdate = new WalletDTO();
        HashMap coinPriceForUpdate = new HashMap();
        coinPriceForUpdate.put("USD",100.0);
        walletDTOBodyForUpdate.setCoinPrice(coinPriceForUpdate);

        boolean throwsAnError = false;
        try {
            ResponseEntity<WalletDTO> resWalletUpdate = connectionToApi.connect(BASE_URL + "/wallets/" + walletDTOCreated.getId(), HttpMethod.PUT, WalletDTO.class, walletDTOBodyForUpdate);
        }catch (HttpClientErrorException.BadRequest ex){
            throwsAnError=true;
        }

        Assert.assertTrue(throwsAnError);
        deleteCustomWallet(walletDTOCreated.getId());

    }

    @Test
    public void updateWalletWithNotExistentCoin(){

        String nameWalletForTest = "Wallet for testing";
        HashMap coinPriceForTest = new HashMap();
        coinPriceForTest.put("ARS",1000.0);

        WalletDTO walletDTO = new WalletDTO();
        walletDTO.setName(nameWalletForTest);
        walletDTO.setCoinPrice(coinPriceForTest);

        ConnectionToApi connectionToApi = new ConnectionToApi();
        ResponseEntity<WalletDTO> resCreatedWallet = connectionToApi.connect(BASE_URL+"/wallets", HttpMethod.POST,WalletDTO.class,walletDTO);
        WalletDTO walletDTOCreated = resCreatedWallet.getBody();

        //update
        WalletDTO walletDTOBodyForUpdate = new WalletDTO();
        HashMap coinPriceForUpdate = new HashMap();
        walletDTOBodyForUpdate.setName("Wallet Updated");
        coinPriceForUpdate.put("adsadad",100.0);
        walletDTOBodyForUpdate.setCoinPrice(coinPriceForUpdate);

        boolean throwsAnError = false;
        try {
            ResponseEntity<WalletDTO> resWalletUpdate = connectionToApi.connect(BASE_URL + "/wallets/" + walletDTOCreated.getId(), HttpMethod.PUT, WalletDTO.class, walletDTOBodyForUpdate);
        }catch (HttpClientErrorException.BadRequest ex){
            throwsAnError=true;
        }

        Assert.assertTrue(throwsAnError);
        deleteCustomWallet(walletDTOCreated.getId());

    }


    @Test
    public void deleteWallet(){

        String nameWalletForTest = "Wallet for testing";
        HashMap coinPriceForTest = new HashMap();
        coinPriceForTest.put("ARS",1000.0);

        WalletDTO walletDTO = new WalletDTO();
        walletDTO.setName(nameWalletForTest);
        walletDTO.setCoinPrice(coinPriceForTest);

        ConnectionToApi connectionToApi = new ConnectionToApi();

        ResponseEntity<WalletDTO> resCreatedWallet = connectionToApi.connect(BASE_URL+"/wallets", HttpMethod.POST,WalletDTO.class,walletDTO);
        WalletDTO walletDTOCreated = resCreatedWallet.getBody();

        ResponseEntity<WalletDTO> resWalletById = connectionToApi.connect(BASE_URL+"/wallets/"+walletDTOCreated.getId(), HttpMethod.GET,WalletDTO.class,null);
        Assert.assertTrue(resWalletById.getStatusCode() == HttpStatus.OK);

        ResponseEntity<DeleteResponseDTO> resDelete = connectionToApi.connect(BASE_URL+"/wallets/"+walletDTOCreated.getId(), HttpMethod.DELETE,DeleteResponseDTO.class,null);

        DeleteResponseDTO deleteResponseDTO = resDelete.getBody();

        Assert.assertTrue(deleteResponseDTO.getId().equals(walletDTOCreated.getId()));

        boolean throwsAnError = false;

        try{
            connectionToApi.connect(BASE_URL+"/wallets/"+walletDTOCreated.getId(),HttpMethod.GET,WalletDTO.class,null);
        }catch (HttpClientErrorException.NotFound ex){
            throwsAnError=true;
        }

        Assert.assertTrue(throwsAnError);
    }

    public void deleteCustomWallet(Long id){
        ConnectionToApi connectionToApi = new ConnectionToApi();
        connectionToApi.connect(BASE_URL+"/wallets/"+id, HttpMethod.DELETE,WalletDTO.class,null);

    }
}
