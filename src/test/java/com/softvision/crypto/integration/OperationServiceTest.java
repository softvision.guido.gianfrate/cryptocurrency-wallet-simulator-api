package com.softvision.crypto.integration;

import com.softvision.crypto.wallets.dto.BuyCurrencyDTO;
import com.softvision.crypto.wallets.dto.TransferCurrencyDTO;
import com.softvision.crypto.wallets.dto.TransferResumeDTO;
import com.softvision.crypto.wallets.dto.WalletDTO;
import com.softvision.crypto.wallets.helpers.ConnectionToApi;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;
import java.util.HashMap;

@RunWith(SpringRunner.class)
public class OperationServiceTest {

    String BASE_URL = "http://localhost:8080/api";
    @Test
    public void buyCurrency(){

        WalletDTO walletDTO = createCustomWallet();
        BuyCurrencyDTO buyCurrencyDTO = new BuyCurrencyDTO();
        buyCurrencyDTO.setCoinToBuy("ARS");
        buyCurrencyDTO.setCoinToPay("USD");
        buyCurrencyDTO.setMountToBuy(5000.0);
        ConnectionToApi connectionToApi = new ConnectionToApi();
        ResponseEntity<WalletDTO> resp = connectionToApi.connect(BASE_URL+"/wallets/"+walletDTO.getId()+"/currency/buy",HttpMethod.POST,WalletDTO.class,buyCurrencyDTO);
        WalletDTO walletDTO1AfterBuy = resp.getBody();
        Assert.assertTrue(walletDTO.getCoinPrice().get("ARS")==null);
        Assert.assertTrue(walletDTO.getCoinPrice().get("USD") > walletDTO1AfterBuy.getCoinPrice().get("USD"));
        deleteCustomWallet(walletDTO.getId());
    }
    @Test
    public void buyCurrencyWithNotExistentCurrency(){

        WalletDTO walletDTO = createCustomWallet();
        BuyCurrencyDTO buyCurrencyDTO = new BuyCurrencyDTO();
        buyCurrencyDTO.setCoinToBuy("adhasdh");
        buyCurrencyDTO.setCoinToPay("USD");
        buyCurrencyDTO.setMountToBuy(5000.0);
        ConnectionToApi connectionToApi = new ConnectionToApi();
        boolean throwsAnError = false;
        try {
            ResponseEntity<WalletDTO> resp = connectionToApi.connect(BASE_URL + "/wallets/" + walletDTO.getId() + "/currency/buy", HttpMethod.POST, WalletDTO.class, buyCurrencyDTO);
        }catch (HttpClientErrorException.BadRequest ex){
            throwsAnError = true;
        }
        Assert.assertTrue(throwsAnError);
        deleteCustomWallet(walletDTO.getId());
    }

    @Test
    public void buyCurrencyWithNotHadMoney(){

        WalletDTO walletDTO = createCustomWallet();
        BuyCurrencyDTO buyCurrencyDTO = new BuyCurrencyDTO();
        buyCurrencyDTO.setCoinToBuy("BTC");
        buyCurrencyDTO.setCoinToPay("ARS");
        buyCurrencyDTO.setMountToBuy(5000.0);
        ConnectionToApi connectionToApi = new ConnectionToApi();
        boolean throwsAnError = false;
        try {
            ResponseEntity<WalletDTO> resp = connectionToApi.connect(BASE_URL + "/wallets/" + walletDTO.getId() + "/currency/buy", HttpMethod.POST, WalletDTO.class, buyCurrencyDTO);
        }catch (HttpClientErrorException.BadRequest ex){
            throwsAnError = true;
        }
        Assert.assertTrue(throwsAnError);
        deleteCustomWallet(walletDTO.getId());
    }

    @Test
    public void buyCurrencyWithNotEnoughMoney(){

        WalletDTO walletDTO = createCustomWallet();
        BuyCurrencyDTO buyCurrencyDTO = new BuyCurrencyDTO();
        buyCurrencyDTO.setCoinToBuy("BTC");
        buyCurrencyDTO.setCoinToPay("USD");
        buyCurrencyDTO.setMountToBuy(5000.0);
        ConnectionToApi connectionToApi = new ConnectionToApi();
        boolean throwsAnError = false;
        try {
            ResponseEntity<WalletDTO> resp = connectionToApi.connect(BASE_URL + "/wallets/" + walletDTO.getId() + "/currency/buy", HttpMethod.POST, WalletDTO.class, buyCurrencyDTO);
        }catch (HttpClientErrorException.Conflict ex){
            throwsAnError = true;
        }
        Assert.assertTrue(throwsAnError);
        deleteCustomWallet(walletDTO.getId());
    }
    @Test
    public void buyCurrencyWithWalletNotExistent(){

        WalletDTO walletDTO = createCustomWallet();
        deleteCustomWallet(walletDTO.getId());
        BuyCurrencyDTO buyCurrencyDTO = new BuyCurrencyDTO();
        buyCurrencyDTO.setCoinToBuy("BTC");
        buyCurrencyDTO.setCoinToPay("USD");
        buyCurrencyDTO.setMountToBuy(5000.0);

        ConnectionToApi connectionToApi = new ConnectionToApi();
        boolean throwsAnError = false;
        try {
            ResponseEntity<WalletDTO> resp = connectionToApi.connect(BASE_URL + "/wallets/" + walletDTO.getId() + "/currency/buy", HttpMethod.POST, WalletDTO.class, buyCurrencyDTO);
        }catch (HttpClientErrorException.NotFound ex){
            throwsAnError = true;
        }
        Assert.assertTrue(throwsAnError);
    }

    @Test
    public void transferCurrencyWithSameCurrency(){
        ConnectionToApi connectionToApi = new ConnectionToApi();

        WalletDTO walletDTO1 = createCustomWallet();
        WalletDTO walletDTO2 = createCustomWallet();

        TransferCurrencyDTO transferCurrencyDTO = new TransferCurrencyDTO();
        transferCurrencyDTO.setIdWallet(walletDTO2.getId());
        transferCurrencyDTO.setCoinToTransfer("USD");
        transferCurrencyDTO.setCoinThatWillReceive("USD");
        transferCurrencyDTO.setMountToTransfer(50.0);

        ResponseEntity<TransferResumeDTO> resp = connectionToApi.connect(BASE_URL+"/wallets/"+walletDTO1.getId()+"/currency/transfer",HttpMethod.POST, TransferResumeDTO.class,transferCurrencyDTO);

        TransferResumeDTO transferResumeDTOAfterTransaction = resp.getBody();

        WalletDTO walletDTO1AfterTransaction = transferResumeDTOAfterTransaction.getYourWallet();
        WalletDTO walletDTO2AfterTransaction = transferResumeDTOAfterTransaction.getReceiver();

        Assert.assertTrue(walletDTO1AfterTransaction.getCoinPrice().get("USD") < walletDTO1.getCoinPrice().get("USD"));
        Assert.assertTrue(walletDTO2AfterTransaction.getCoinPrice().get("USD") > walletDTO2.getCoinPrice().get("USD"));
        Assert.assertTrue(walletDTO1AfterTransaction.getName().equals(walletDTO1.getName()));
        Assert.assertTrue(walletDTO2AfterTransaction.getName().equals(walletDTO2.getName()));

        deleteCustomWallet(walletDTO1.getId());
        deleteCustomWallet(walletDTO2.getId());
    }
    @Test
    public void transferCurrencyWithDifferentCurrency(){
        ConnectionToApi connectionToApi = new ConnectionToApi();

        WalletDTO walletDTO1 = createCustomWallet();
        WalletDTO walletDTO2 = createCustomWallet();

        TransferCurrencyDTO transferCurrencyDTO = new TransferCurrencyDTO();
        transferCurrencyDTO.setIdWallet(walletDTO2.getId());
        transferCurrencyDTO.setCoinToTransfer("USD");
        transferCurrencyDTO.setCoinThatWillReceive("EUR");
        transferCurrencyDTO.setMountToTransfer(5.0);

        ResponseEntity<TransferResumeDTO> resp = connectionToApi.connect(BASE_URL+"/wallets/"+walletDTO1.getId()+"/currency/transfer",HttpMethod.POST, TransferResumeDTO.class,transferCurrencyDTO);

        TransferResumeDTO transferResumeDTOAfterTransaction = resp.getBody();

        WalletDTO walletDTO1AfterTransaction = transferResumeDTOAfterTransaction.getYourWallet();
        WalletDTO walletDTO2AfterTransaction = transferResumeDTOAfterTransaction.getReceiver();

        Assert.assertTrue(walletDTO1AfterTransaction.getCoinPrice().get("USD") < walletDTO1.getCoinPrice().get("USD"));
        Assert.assertTrue(walletDTO2AfterTransaction.getCoinPrice().get("USD").equals(walletDTO2.getCoinPrice().get("USD")));
        Assert.assertTrue(walletDTO2AfterTransaction.getCoinPrice().get("EUR") != null);
        Assert.assertTrue(walletDTO1AfterTransaction.getName().equals(walletDTO1.getName()));
        Assert.assertTrue(walletDTO2AfterTransaction.getName().equals(walletDTO2.getName()));

        deleteCustomWallet(walletDTO1.getId());
        deleteCustomWallet(walletDTO2.getId());
    }


    @Test
    public void transferCurrencyWithWallet1NotExistent(){
        ConnectionToApi connectionToApi = new ConnectionToApi();

        WalletDTO walletDTO1 = createCustomWallet();
        deleteCustomWallet(walletDTO1.getId());

        WalletDTO walletDTO2 = createCustomWallet();

        TransferCurrencyDTO transferCurrencyDTO = new TransferCurrencyDTO();
        transferCurrencyDTO.setIdWallet(walletDTO2.getId());
        transferCurrencyDTO.setCoinToTransfer("USD");
        transferCurrencyDTO.setCoinThatWillReceive("USD");
        transferCurrencyDTO.setMountToTransfer(50.0);

        boolean throwsError = false;
        try {
            ResponseEntity<TransferResumeDTO> resp = connectionToApi.connect(BASE_URL + "/wallets/" + walletDTO1.getId() + "/currency/transfer", HttpMethod.POST, TransferResumeDTO.class, transferCurrencyDTO);
        }catch (HttpClientErrorException.NotFound ex){
            throwsError = true;
        }
        Assert.assertTrue(throwsError);
        deleteCustomWallet(walletDTO2.getId());
    }

    @Test
    public void transferCurrencyWithWallet2NotExistent(){
        ConnectionToApi connectionToApi = new ConnectionToApi();

        WalletDTO walletDTO1 = createCustomWallet();
        WalletDTO walletDTO2 = createCustomWallet();
        deleteCustomWallet(walletDTO2.getId());

        TransferCurrencyDTO transferCurrencyDTO = new TransferCurrencyDTO();
        transferCurrencyDTO.setIdWallet(walletDTO2.getId());
        transferCurrencyDTO.setCoinToTransfer("USD");
        transferCurrencyDTO.setCoinThatWillReceive("EUR");
        transferCurrencyDTO.setMountToTransfer(5.0);

        boolean throwsError = false;

        try {
            ResponseEntity<TransferResumeDTO> resp = connectionToApi.connect(BASE_URL + "/wallets/" + walletDTO1.getId() + "/currency/transfer", HttpMethod.POST, TransferResumeDTO.class, transferCurrencyDTO);
        }catch (HttpClientErrorException.NotFound ex){
            throwsError = true;
        }
        Assert.assertTrue(throwsError);
        deleteCustomWallet(walletDTO1.getId());

    }

    @Test
    public void transferCurrencyWithNotExistentCoinToTransfer(){
        ConnectionToApi connectionToApi = new ConnectionToApi();

        WalletDTO walletDTO1 = createCustomWallet();
        WalletDTO walletDTO2 = createCustomWallet();

        TransferCurrencyDTO transferCurrencyDTO = new TransferCurrencyDTO();
        transferCurrencyDTO.setIdWallet(walletDTO2.getId());
        transferCurrencyDTO.setCoinToTransfer("dasdsa");
        transferCurrencyDTO.setCoinThatWillReceive("USD");
        transferCurrencyDTO.setMountToTransfer(50.0);
        boolean throwsError = false;
        try {
            ResponseEntity<TransferResumeDTO> resp = connectionToApi.connect(BASE_URL + "/wallets/" + walletDTO1.getId() + "/currency/transfer", HttpMethod.POST, TransferResumeDTO.class, transferCurrencyDTO);
        }catch (HttpClientErrorException.Conflict ex){
            throwsError = true;
        }
        Assert.assertTrue(throwsError);
        deleteCustomWallet(walletDTO1.getId());
        deleteCustomWallet(walletDTO2.getId());
    }

    @Test
    public void transferCurrencyWithNotExistentCoinToReceive(){
        ConnectionToApi connectionToApi = new ConnectionToApi();

        WalletDTO walletDTO1 = createCustomWallet();
        WalletDTO walletDTO2 = createCustomWallet();

        TransferCurrencyDTO transferCurrencyDTO = new TransferCurrencyDTO();
        transferCurrencyDTO.setIdWallet(walletDTO2.getId());
        transferCurrencyDTO.setCoinToTransfer("USD");
        transferCurrencyDTO.setCoinThatWillReceive("kdjaosdjsko");
        transferCurrencyDTO.setMountToTransfer(50.0);
        boolean throwsError = false;
        try {
            ResponseEntity<TransferResumeDTO> resp = connectionToApi.connect(BASE_URL + "/wallets/" + walletDTO1.getId() + "/currency/transfer", HttpMethod.POST, TransferResumeDTO.class, transferCurrencyDTO);
        }catch (HttpClientErrorException.BadRequest ex){
            throwsError = true;
        }
        Assert.assertTrue(throwsError);
        deleteCustomWallet(walletDTO1.getId());
        deleteCustomWallet(walletDTO2.getId());
    }

    @Test
    public void transferCurrencyWithNotEnoughMoney(){
        ConnectionToApi connectionToApi = new ConnectionToApi();

        WalletDTO walletDTO1 = createCustomWallet();
        WalletDTO walletDTO2 = createCustomWallet();

        TransferCurrencyDTO transferCurrencyDTO = new TransferCurrencyDTO();
        transferCurrencyDTO.setIdWallet(walletDTO2.getId());
        transferCurrencyDTO.setCoinToTransfer("USD");
        transferCurrencyDTO.setCoinThatWillReceive("USD");
        transferCurrencyDTO.setMountToTransfer(52734923847923840.0);
        boolean throwsError = false;
        try {
            ResponseEntity<TransferResumeDTO> resp = connectionToApi.connect(BASE_URL + "/wallets/" + walletDTO1.getId() + "/currency/transfer", HttpMethod.POST, TransferResumeDTO.class, transferCurrencyDTO);
        }catch (HttpClientErrorException.Conflict ex){
            throwsError = true;
        }
        Assert.assertTrue(throwsError);
        deleteCustomWallet(walletDTO1.getId());
        deleteCustomWallet(walletDTO2.getId());
    }
    public WalletDTO createCustomWallet(){
        String nameWalletForTest = "Wallet for testing";
        HashMap coinPriceForTest = new HashMap();
        coinPriceForTest.put("USD",1000.0);
        WalletDTO walletDTO = new WalletDTO();
        walletDTO.setName(nameWalletForTest);
        walletDTO.setCoinPrice(coinPriceForTest);
        ConnectionToApi connectionToApi = new ConnectionToApi();
        ResponseEntity<WalletDTO> res = connectionToApi.connect(BASE_URL+"/wallets", HttpMethod.POST,WalletDTO.class,walletDTO);
        WalletDTO walletDTOForTest = res.getBody();
        return walletDTOForTest;
    }
    public void deleteCustomWallet(Long id){
        ConnectionToApi connectionToApi = new ConnectionToApi();
        connectionToApi.connect(BASE_URL+"/wallets/"+id, HttpMethod.DELETE,WalletDTO.class,null);
    }
}