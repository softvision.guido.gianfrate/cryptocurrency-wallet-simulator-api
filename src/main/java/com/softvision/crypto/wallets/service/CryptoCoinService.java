package com.softvision.crypto.wallets.service;

import com.softvision.crypto.wallets.dto.PaginationResponse;
import com.softvision.crypto.wallets.exception.BadInputException;
import com.softvision.crypto.wallets.helpers.ConnectionToApi;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

@Service
public class CryptoCoinService {

    String BASE_URL;
    String convertFromURL;
    String convertToURL;
    int maxLimit = 100;
    public CryptoCoinService(){
        BASE_URL = "https://min-api.cryptocompare.com/data";
        convertFromURL = "?fsym=";
        convertToURL = "&tsyms=";
    }

    /**
     * @param offset a number higher  or equals to 0
     * @param limit a number between 0 and 100
     * @param turnOverCoin is the coin to reference
     * @return The values paginated by the params
     */
    public PaginationResponse getValuesOfCoinWithPagination(int offset,int limit,String turnOverCoin ){

        if(limit>maxLimit)throw new BadInputException("The max limit is 100");
        ArrayList<String> allNames = getAllNamesOfCryptoCoins();
        int total = allNames.size();
        int subListFrom = offset;
        int subListTo = offset+limit;
        if(subListTo> total)subListTo=total;

        String wantedCoinNames = parseStringArrayListToStringSplitByComas(allNames.subList(subListFrom,subListTo));

        LinkedHashMap keyCoinValueValue = getValuesOfCoins(turnOverCoin, wantedCoinNames);
        PaginationResponse paginationResponse = new PaginationResponse();
        paginationResponse.setTotal(total);
        paginationResponse.setCoins(keyCoinValueValue);
        paginationResponse.setOffset(offset);
        paginationResponse.setLimit(limit);

        return paginationResponse;
    }


    /**
     * @return Do a request and take the useful information, the names of
     * coins, and returns all in a array
     */

    @Cacheable(value = "allCoins", key = "'allCoins'")
    public ArrayList<String> getAllNamesOfCryptoCoins(){
        ConnectionToApi connectionToApi = new ConnectionToApi();
        ResponseEntity<LinkedHashMap> res =  connectionToApi.connect(BASE_URL+"/all/coinlist", HttpMethod.GET, LinkedHashMap.class,null);
        LinkedHashMap totalResponse =  res.getBody();
        LinkedHashMap objectWithArrayListOfCoins = (LinkedHashMap) totalResponse.get("Data");
        ArrayList allNamesList = new ArrayList();
        allNamesList.addAll(objectWithArrayListOfCoins.keySet());
        return allNamesList;
    }


    /**
     * @param turnOverCoin Is the coin to reference
     * @param listCoinToConvert this param can be one name of coin or more coins separated by comas
     * @return a map with key CoinToConvert and value (the value of 1 'turnOverCoin' according the 'coinToConvert')
     */

    public LinkedHashMap getValuesOfCoins(String turnOverCoin,String listCoinToConvert){
            ConnectionToApi connectionToApi = new ConnectionToApi();
            ResponseEntity<LinkedHashMap> res = connectionToApi.connect(BASE_URL + "/price" + convertFromURL + turnOverCoin + convertToURL + listCoinToConvert, HttpMethod.GET, LinkedHashMap.class, null);
            LinkedHashMap response = res.getBody();
            if(response.get("Response") != null && response.get("Response").equals("Error"))throw new BadInputException("The currency "+ turnOverCoin+ " not exist or any of the following: "+listCoinToConvert );
        return response;
    }

    /**
     * @param entryCoins: Is an arrayList of Strings
     * @return returns a only one String, is the strings of the entryCoins split by comas.
     */
    public String parseStringArrayListToStringSplitByComas(List<String> entryCoins){
            String stringSplitedByComas ="";
            for(String tempCoinName: entryCoins){
                stringSplitedByComas = stringSplitedByComas + tempCoinName +",";
            }
            return stringSplitedByComas;
        }


}
