package com.softvision.crypto.wallets.helpers;


import com.softvision.crypto.wallets.exception.BadInputException;
import com.softvision.crypto.wallets.service.CryptoCoinService;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Set;

public class VerifyCoinInPostUser {

    public void verify(Set coinPrice){

        CryptoCoinService cryptoCoinService = new CryptoCoinService();

        ArrayList<String> coinNames = new ArrayList();

        coinNames.addAll(coinPrice);

        ArrayList coinGettedFromHttpResponse = new ArrayList();

        ArrayList notExistentCoins = new ArrayList();


        String parsedStrings = cryptoCoinService.parseStringArrayListToStringSplitByComas(coinNames);
        LinkedHashMap response = cryptoCoinService.getValuesOfCoins("USD",parsedStrings);

        coinGettedFromHttpResponse.addAll(response.keySet());

        for(String tempCoin: coinNames ){

            if(!coinGettedFromHttpResponse.contains(tempCoin)){
                notExistentCoins.add(tempCoin);
            }
        }

        if(notExistentCoins.size() > 0)throw new BadInputException("The following coins no exist: "+notExistentCoins.toString());
    }






}
