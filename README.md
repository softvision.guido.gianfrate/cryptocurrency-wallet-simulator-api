# CryptoCurrency Wallet Simulator API
#### Developer: Guido Agustin Gianfrate

### Description of the API


The simulator design will be a simple POC, it will allow CRUD operations of the Wallets, and allow for the transfer operations in, and between wallets.  The prices and available symbols have to be obtained from the APIs exposed in CryptoCompare​ Services (e.g.: ​Multiple Symbols Service​), and have to be obtained when doing the exchanges of the currency.  Bear in mind that the described input and output result content, are not limited to the specified elements, but the specified values should be present.  There is no security implemented for the API, so all the operations are unrestrained to simplify the implementation. 

### Clone the repository 


```bash
https://gitlab.com/softvision.guido.gianfrate/cryptocurrency-wallet-simulator-api.git
```
# Available Endpoints:

You can see the endpoints in the following postman collection:
```bash
https://www.getpostman.com/collections/8f9156722f8f7d5b7fe0
```
This project use interactive swagger documentation, check the following link:
```bash
https://crypto-currency-simulator.herokuapp.com/swagger-ui.html#/
```